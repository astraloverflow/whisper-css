whisper.css
=========

[![License][license-img]][license-url]
[![Version][version-img]][version-url]
[![Open Issues][issues-img]][issues-url]
[![Maintenance](https://img.shields.io/maintenance/yes/2016.svg)][issues-url]
[![Join the chat at https://gitter.im/iron16/whisper.css][gitter-img]][gitter-url]

> Tools For Displaying Hidden Information

## installation

**NPM** *(Recommended)*

1. Move into your project directory
2. Run `npm install -S iron16/whisper.css`
3. Add `<link rel="stylesheet" href="node_modules/whisper.css/whisper.css" />` to the `<head>` of your web page

**Bower**

1. Move into your project directory
2. Run `bower install iron16/whisper.css`
3. Add `<link rel="stylesheet" href="bower_components/whisper.css/whisper.css" />` to the `<head>` of your web page

## Usage
Once installed, everything works without any configuration needed.

**Coming Soon**

[license-url]: https://github.com/iron16/whisper.css/blob/master/LICENSE
[license-img]: https://img.shields.io/github/license/iron16/whisper.css.svg

[version-url]: https://github.com/iron16/whisper.css/releases
[version-img]: https://img.shields.io/github/release/iron16/whisper.css.svg

[issues-url]: https://github.com/iron16/whisper.css/issues
[issues-img]: http://img.shields.io/github/issues/iron16/whisper.css.svg

[gitter-url]: https://gitter.im/iron16/whisper.css
[gitter-img]: https://badges.gitter.im/iron16/whisper.css.svg
